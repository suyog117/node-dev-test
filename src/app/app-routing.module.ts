import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ProductCreateComponent } from './components/product-create/product-create.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { productEditComponent } from './components/product-edit/product-edit.component';
import { CategoryCreateComponent } from './components/category-create/category-create.component';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { categoryEditComponent } from './components/category-edit/category-edit.component';
// import { ProductListComponent } from './components/product-create/product-list.component';
// import {ProductEditComponent } from './components/product-create/product-edit.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'product-list' },
  { path: 'create-product', component: ProductCreateComponent },
  { path: 'create-category', component: CategoryCreateComponent },
  { path: 'product-list', component: ProductListComponent },
  { path: 'edit-product/:id', component: productEditComponent },
  { path: 'category-list', component: CategoryListComponent },
  { path: 'edit-category/:id', component: categoryEditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
