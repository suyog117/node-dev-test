import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../service/api.service';


const employee = [
  {
    "_id":1,
    "name": "Suyog",
    "email":"Suyog@gmail.com",
    "designation":"Developer",
    "phoneNumber":"12434",
  }
]
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
})

export class EmployeeListComponent implements OnInit {
  Employee: any = [];

  constructor(private apiService: ApiService) {
    this.readEmployee();
  }

  ngOnInit() {}

  readEmployee() {
    this.Employee = employee;

    // this.apiService.getEmployees().subscribe((data) => {
    //   this.Employee = employee;
    // });
  }

  removeEmployee(employee, index) {
    if (window.confirm('Are you sure?')) {
      this.apiService.deleteEmployee(employee._id).subscribe((data) => {
        this.Employee.splice(index, 1);
      });
    }
  }
}
