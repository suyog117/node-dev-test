import { Router } from '@angular/router';
import { ApiService } from '../../service/api.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.css'],
})

export class CategoryCreateComponent implements OnInit {
  submitted = false;
  categoryForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private apiService: ApiService
  ) {
    this.mainForm();
  }

  ngOnInit() {}

  mainForm() {
    this.categoryForm = this.fb.group({
      category: ['', [Validators.required]]
    });
  }


  // Getter to access form control
  get myForm() {
    return this.categoryForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.categoryForm.valid) {
      return false;
    } else {
      return this.apiService.createCategory(this.categoryForm.value).subscribe({
        complete: () => {
          console.log('Category successfully created!')
            this.ngZone.run(() => this.router.navigateByUrl('/category-list'));
        },
        error: (e) => {
          console.log(e);
        },
      });
    }
  }
}
