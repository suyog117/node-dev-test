import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css'],
})
export class CategoryListComponent implements OnInit {
  categories: any = [];
  Loading: Boolean = false;
  page: number = 1;
  nextPage: number;
  pageSize: number;
  collectionSize: number;

  constructor(private apiService: ApiService) {
    this.readCategories();
  }

  ngOnInit() {}

  readCategories() {
    this.apiService.getCategory(1).subscribe((data) => {
      console.log(data)
      this.page = data['page'];
      this.pageSize = data['limit'];
      this.collectionSize = data['total'];
      this.categories = data['docs'];
      this.Loading = true;
      console.log(this.categories);
    });
  }

  nextCategoryPage() {
    this.apiService.getCategory(this.page).subscribe((data) => {
      this.page = data['page'];
      console.log('page', data['page']);
      this.pageSize = data['limit'];
      console.log('pageSize',this.pageSize);
      this.collectionSize = data['total'];
      this.categories = data['docs'];
    });
  }

  removeCategory(category,i) {
    if (window.confirm('Are you sure?')) {
      this.apiService.deleteCategory(category._id).subscribe((data) => {
        this.categories.splice(i,1)
      });
    }
  }
}
