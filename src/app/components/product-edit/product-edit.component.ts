import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../service/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css'],
})
export class productEditComponent implements OnInit {
  submitted = false;
  editForm: FormGroup;
  productData: any = [];
  Categories: any = [];
  productId: any;

  constructor(
    public fb: FormBuilder,
    private actRoute: ActivatedRoute,
    private apiService: ApiService,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.updateproduct();
    this.GetCategoryList();
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.getproduct(id);
    this.productId = id;
    this.editForm = this.fb.group({
      _id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      sku: ['', [Validators.required]],
      description: ['', [Validators.required]],
      url: ['', [Validators.required]],
      category: ['', [Validators.required]],
      price: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      weight: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
    });
  }

  GetCategoryList() {
    this.http.get(`http://localhost:4000/v1/Category/`).subscribe((data) => {
      this.Categories = data['docs'];
    });
  }

  // Choose category with select dropdown
  updateProfile(e) {
    this.editForm.get('category').setValue(e, {
      onlySelf: true,
    });
  }

  // Getter to access form control
  get myForm() {
    return this.editForm.controls;
  }

  getproduct(id) {
    this.apiService.getProduct(id).subscribe((data) => {
      this.editForm.setValue({
        _id: data['_id'],
        name: data['name'],
        brand: data['brand'],
        sku: data['sku'],
        description: data['description'],
        url: data['url'],
        category: data['category'],
        price: data['price'],
        weight: data['price'],
      });
    });
  }

  updateproduct() {
    this.editForm = this.fb.group({
      _id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      sku: ['', [Validators.required]],
      description: ['', [Validators.required]],
      url: ['', [Validators.required]],
      category: ['', [Validators.required]],
      price: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      weight: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
    });
  }

  onSubmit() {
    this.submitted = true;
    this.editForm['_id'] = this.productId;
    if (!this.editForm.valid) {
      return false;
    } else {
      if (window.confirm('Are you sure?')) {
        let id = this.actRoute.snapshot.paramMap.get('id');
        this.apiService.updateProduct(this.editForm.value).subscribe({
          complete: () => {
            this.router.navigateByUrl('/product-list');
            console.log('Content updated successfully!');
          },
          error: (e) => {
            console.log(e);
          },
        });
      }
    }
  }
}
