import { Router } from '@angular/router';
import { ApiService } from './../../service/api.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css'],
})
export class ProductCreateComponent implements OnInit {
  submitted = false;
  productForm: FormGroup;
  Categories: any = [];

  constructor(
    public fb: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private apiService: ApiService,
    private http: HttpClient
  ) {
    this.mainForm();
    this.GetCategoryList();
  }

  ngOnInit() {}

  GetCategoryList() {
    this.http.get(`http://localhost:4000/v1/Category/`).subscribe((data) => {
      this.Categories = data["docs"];
    });
  }

  mainForm() {
    this.productForm = this.fb.group({
      name: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      sku: ['', [Validators.required]],
      description: ['', [Validators.required]],
      url: ['', [Validators.required]],
      category: ['', [Validators.required]],
      price: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      weight: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
    });
  }

  // Choose category with select dropdown
  updateProfile(e) {
    this.productForm.get('category').setValue(e, {
      onlySelf: true,
    });
  }

  // Getter to access form control
  get myForm() {
    return this.productForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.productForm.valid) {
      return false;
    } else {
      return this.apiService.createProduct(this.productForm.value).subscribe({
        complete: () => {
          console.log('Product successfully created!'),
            this.ngZone.run(() => this.router.navigateByUrl('/product-list'));
        },
        error: (e) => {
          console.log(e);
        },
      });
    }
  }
}
