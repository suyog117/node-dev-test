import { ComponentFixture, TestBed } from '@angular/core/testing';

import { categoryEditComponent } from './category-edit.component';

describe('categoryEditComponent', () => {
  let component: categoryEditComponent;
  let fixture: ComponentFixture<categoryEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ categoryEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(categoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
