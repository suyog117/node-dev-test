import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../service/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css'],
})
export class categoryEditComponent implements OnInit {
  submitted = false;
  editForm: FormGroup;
  categoryData: any = [];
  Categories: any = [];
  categoryId: any;

  constructor(
    public fb: FormBuilder,
    private actRoute: ActivatedRoute,
    private apiService: ApiService,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.updatecategory();
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.getcategory(id);
    this.categoryId = id;
    this.editForm = this.fb.group({
      _id: ['', [Validators.required]],
      category: ['', [Validators.required]],
    });
  }


  // Getter to access form control
  get myForm() {
    return this.editForm.controls;
  }

  getcategory(id) {
    this.apiService.getCategoryById(id).subscribe((data) => {
      this.editForm.setValue({
        _id: data['_id'],
        category: data['category']
      });
    });
  }

  updatecategory() {
    this.editForm = this.fb.group({
      _id: ['', [Validators.required]],
      category: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.submitted = true;
    this.editForm['_id'] = this.categoryId;
    if (!this.editForm.valid) {
      return false;
    } else {
      if (window.confirm('Are you sure?')) {
        this.apiService.updateCategory(this.editForm.value).subscribe({
          complete: () => {
            this.router.navigateByUrl('/category-list');
            console.log('Content updated successfully!');
          },
          error: (e) => {
            console.log(e);
          },
        });
      }
    }
  }
}
