import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../service/api.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
  Products: any = [];
  Loading: Boolean = false;
  page: number = 1;
  nextPage: number;
  pageSize: number;
  collectionSize: number;

  constructor(private apiService: ApiService) {
    this.readProducts();
  }

  ngOnInit() {}

  readProducts() {
    this.apiService.getProducts(1).subscribe((data) => {
      this.page = data['page'];
      this.pageSize = data['limit'];
      this.collectionSize = data['total'];
      this.Products = data['docs'];
      this.Loading = true;
      console.log(this.pageSize);
    });
  }

  nextProductPage() {
    this.apiService.getProducts(this.page).subscribe((data) => {
      this.page = data['page'];
      console.log('page', data['page']);
      this.pageSize = data['limit'];
      console.log('pageSize',this.pageSize);
      this.collectionSize = data['total'];
      this.Products = data['docs'];
    });
  }

  removeProduct(product,i) {
    if (window.confirm('Are you sure?')) {
      this.apiService.deleteProduct(product._id).subscribe((data) => {
        this.Products.splice(i,1)
      });
    }
  }
}
