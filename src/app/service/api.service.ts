import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  baseUri: string = 'http://localhost:4000/api';
  productUri: string = 'http://localhost:4000/v1/product';
  categoryUri: string = 'http://localhost:4000/v1/category';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) {}

  // Create
  createEmployee(data): Observable<any> {
    let url = `${this.baseUri}/create`;
    return this.http.post(url, data).pipe(catchError(this.errorMgmt));
  }

  // Get all employees
  getEmployees() {
    return this.http.get(`${this.baseUri}`);
  }

  // Get employee
  getEmployee(id): Observable<any> {
    let url = `${this.baseUri}/read/${id}`;
    return this.http.get(url, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {};
      }),
      catchError(this.errorMgmt)
    );
  }

  // Update employee
  updateEmployee(id, data): Observable<any> {
    let url = `${this.baseUri}/update/${id}`;
    return this.http
      .put(url, data, { headers: this.headers })
      .pipe(catchError(this.errorMgmt));
  }

  // Delete employee
  deleteEmployee(id): Observable<any> {
    let url = `${this.baseUri}/delete/${id}`;
    return this.http
      .delete(url, { headers: this.headers })
      .pipe(catchError(this.errorMgmt));
  }

  //create Product
  createProduct(data): Observable<any> {
    let url = `${this.productUri}/create`;
    return this.http.post(url, data).pipe(catchError(this.errorMgmt));
  }

  // Get all products
  getProducts(page) {
    return this.http.get(`${this.productUri}?page=${page}`);
  }

  // Get product
  getProduct(id): Observable<any> {
    let url = `${this.productUri}/get/${id}`;
    return this.http.get(url, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {};
      }),
      catchError(this.errorMgmt)
    );
  }

  // Update product
  updateProduct(data): Observable<any> {
    let url = `${this.productUri}/update`;
    return this.http
      .put(url, data, { headers: this.headers })
      .pipe(catchError(this.errorMgmt));
  }

  // Delete employee
  deleteProduct(id): Observable<any> {
    let url = `${this.productUri}/delete`;
    return this.http
      .delete(url, { headers: this.headers, body: { productId: id } })
      .pipe(catchError(this.errorMgmt));
  }

  //create Category
  createCategory(data): Observable<any> {
    let url = `${this.categoryUri}/create`;
    return this.http.post(url, data).pipe(catchError(this.errorMgmt));
  }

  // Get all categories
  getCategory(page) {
    return this.http.get(`${this.categoryUri}?page=${page}`);
  }

  // Get Category
  getCategoryById(id): Observable<any> {
    let url = `${this.categoryUri}/get/${id}`;
    return this.http.get(url, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {};
      }),
      catchError(this.errorMgmt)
    );
  }

  // Update category
  updateCategory(data): Observable<any> {
    let url = `${this.categoryUri}/edit`;
    return this.http
      .put(url, data, { headers: this.headers })
      .pipe(catchError(this.errorMgmt));
  }

  // Delete employee
  deleteCategory(id): Observable<any> {
    let url = `${this.categoryUri}/delete`;
    return this.http
      .delete(url, { headers: this.headers, body: { categoryId: id } })
      .pipe(catchError(this.errorMgmt));
  }

  // Error handling
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }
}
