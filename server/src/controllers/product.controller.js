const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { productService } = require("../services");

const searchCatalog = catchAsync(async (req, res) => {
  let response = await productService.seachCatalog(
    req.query.page,
    req.query.limit,
    req.query.searchTerm,
    req.query.categoryIds,
    req.query.brand,
    req.query.sortRule,
    req.query.sku
  );
  return res.status(httpStatus.OK).send(response);

});


const addProductToCatalog = catchAsync(async (req, res) => {
  let response;
  response = await productService.addProduct(req.body, req.user);
  return res.status(httpStatus.CREATED).send(response);
});

const getProductDetails = catchAsync(async (req, res) => {
  let response = await productService.getProductDetails(req.params.productId);
  return res.status(httpStatus.OK).send(response);
});

const updateProductInCatalog = catchAsync(async (req, res) => {
  let response = await productService.updateProductInCatlog(req.body);
  return res.status(httpStatus.OK).send(response);
});

const deleteProductFromCatalog = catchAsync(async (req, res) => {
  let response = await productService.deleteProductFromCatalog(req.body.productId);
  return res.status(httpStatus.OK).send(response);
});

const deleteMultipleProductFromCatalog = catchAsync(async (req, res) => {
  await productService.deleteMultipleProductFromCatalog(req.body.productIds);
  return res.status(httpStatus.NO_CONTENT).send();
});


module.exports = {
  searchCatalog,
  addProductToCatalog,
  getProductDetails,
  updateProductInCatalog,
  deleteProductFromCatalog,
  deleteMultipleProductFromCatalog
};


