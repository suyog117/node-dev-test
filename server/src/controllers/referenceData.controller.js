const { referenceDataService } = require("../services");
const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");

const getProductCategoryList = catchAsync(async (req, res) => {
  let response = await referenceDataService.getProductCategoryList(
    req.query.page,
    req.query.limit,
    req.query.sortRule
  );
  return res.status(httpStatus.CREATED).send(response);
});

const addProductCategory = catchAsync(async (req, res) => {
  let response;
  response = await referenceDataService.addProductCategory(req.body);
  return res.status(httpStatus.CREATED).send(response);
});

const getProductCategory = catchAsync(async (req, res) => {
  let response;
  response = await referenceDataService.getProductCategory(
    req.params.categoryId
  );
  return res.status(httpStatus.OK).send(response);
});

const updateProductCategory = catchAsync(async (req, res) => {
  let response;
  response = await referenceDataService.updateProductCategory(req.body);
  return res.status(httpStatus.OK).send(response);
});

const deleteProductCategory = catchAsync(async (req, res) => {
  let response;
  response = await referenceDataService.deleteProductCategory(req.body);
  return res.status(httpStatus.OK).send(response);
});

const deleteMultipleProductCatalog = catchAsync(async (req, res) => {
  await referenceDataService.deleteMultipleProductCategory(
    req.body.categoryIds
  );
  return res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  getProductCategoryList,
  addProductCategory,
  getProductCategory,
  updateProductCategory,
  deleteProductCategory,
  deleteMultipleProductCatalog,
};
