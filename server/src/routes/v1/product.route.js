const express = require("express");
const validate = require("../../middlewares/validate");
const productValidation = require("../../validations/product.validation");
const productController = require("../../controllers/product.controller");


const router = express.Router();

router.get(
  "/",
  validate(productValidation.searchCatalog),
  productController.searchCatalog
);

router.post(
  "/create",
  validate(productValidation.addProduct),
  productController.addProductToCatalog
);

router.get(
  "/get/:productId",
  validate(productValidation.getProductDetails),
  productController.getProductDetails
);

router.put(
  "/update",
  validate(productValidation.updateProductInCatalog),
  productController.updateProductInCatalog
);

router.delete(
  "/delete",
  validate(productValidation.deleteProductFromCatalog),
  productController.deleteProductFromCatalog
);


module.exports = router;
