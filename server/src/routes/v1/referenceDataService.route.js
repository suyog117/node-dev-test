const express = require('express');
const validate = require('../../middlewares/validate');
const referenceDataValidation = require('../../validations/referenceData.validation');
const referenceDataController = require('../../controllers/referenceData.controller');

const router = express.Router();

// category routes
router.get('/',validate(referenceDataValidation.getProductCategories), referenceDataController.getProductCategoryList);
router.post('/create',validate(referenceDataValidation.addProductCategory), referenceDataController.addProductCategory);
router.get('/get/:categoryId', validate(referenceDataValidation.getProductCategory), referenceDataController.getProductCategory);
router.put('/edit', validate(referenceDataValidation.updateProductCategory), referenceDataController.updateProductCategory);
router.delete('/delete',validate(referenceDataValidation.deleteProductCategory), referenceDataController.deleteProductCategory);


module.exports = router;
