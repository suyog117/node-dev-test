const express = require("express");
const productRoute = require('./product.route');
const referenceDataService = require("./referenceDataService.route");

const router = express.Router();

const defaultRoutes = [
  {
    path: "/product",
    route: productRoute,
  },
  {
    path: "/category",
    route: referenceDataService,
  }
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

module.exports = router;
