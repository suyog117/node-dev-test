const { Product } = require("../models/product.model");
const Category = require("../models/category.model");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");

const addProduct = async (productObj) => {
  if (!productObj) {
    throw new ApiError(httpStatus.NOT_FOUND, "Please Send Product Information");
  }
  let product = await Product.create(productObj);

  if (!product) {
    throw new ApiError(httpStatus.NOT_FOUND, "Failed To Create Product");
  }
  return product;
};

const seachCatalog = async (
  page,
  limit,
  searchTerm,
  categoryIds,
  brand,
  sortRule,
  sku
) => {
  let searchObj = searchTerm
    ? { name: { $regex: searchTerm, $options: "i" } }
    : {};
  categoryIds ? (searchObj.category = { $in: categoryIds }) : "";
  brand ? (searchObj.brand = brand) : "";
  sku ? (searchObj.store_sku = sku) : "";
  searchObj.active = true; // Only return from active items
  let products = await Product.paginate(searchObj, {
    select: "",
    populate: [{ path: "category", select: "-_id category categoryCode" }],
    sort: sortRule,
    page: page,
    limit: limit,
  });

  let categotySearch = {
    category: { $regex: "^" + searchTerm, $options: "i" },
  };
  category = await Category.paginate(categotySearch, {
    select: "_id category categoryCode",
    sort: sortRule,
    page: page,
    limit: limit,
  });

  if (category && category.docs.length > 0) {
    let categoryObj = {
      categories: category,
    };
    products.docs.unshift(categoryObj);
  }
  if (!products) {
    throw new ApiError(httpStatus.NOT_FOUND, "Failed To Search Product");
  }
  return products;
};

const getProductDetails = async (productId) => {
  let searchQuery = { $and: [{ _id: productId, active: true }] };
  var productDetails = await Product.findOne(searchQuery)
    .populate("category")
    .exec();
  if (!productDetails) {
    throw new ApiError(httpStatus.NOT_FOUND, "Failed To Fetched Product");
  }
  return productDetails;
};

const updateProductInCatlog = async (productObj) => {
  // Store the id and delete it from the received object, to prevent any accidental replacement of id field
  let id = productObj._id;
  if (!id) {
    throw new ApiError(httpStatus.NOT_FOUND, "Failed To Update Product");
  }
  delete productObj._id;

  // Make the update and return the updated document. Also run validators. Mongoose warns only limited validation takes place doing this in update
  let product = await Product.findOneAndUpdate({ _id: id }, productObj, {
    runValidators: true,
    new: true,
  })
    .populate("category")
    .exec();

  if (!product) {
    throw new ApiError(httpStatus.NOT_FOUND, "Failed To Update Product");
  }
  return product;
};

const deleteProductFromCatalog = async (productId) => {
  // Remove the product
  let res = await Product.remove({ _id: productId }).exec();

  // if removal not successful, return failure
  if (!res) {
    throw new ApiError(httpStatus.NOT_FOUND, "Failed To Remove Product");
  }
  return res;
};

const deleteMultipleProductFromCatalog = async (productIds) => {
  // Remove the product
  let products = await Product.deleteMany({ _id: { $in: productIds } });

  // if removal not successful, return failure
  if (!products) {
    throw new ApiError(httpStatus.NOT_FOUND, "Failed To Remove Products");
  }
  return products;
};

module.exports = {
  seachCatalog,
  addProduct,
  getProductDetails,
  updateProductInCatlog,
  deleteProductFromCatalog,
  deleteMultipleProductFromCatalog,
};
