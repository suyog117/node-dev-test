const _ = require("lodash");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");
const ProductCategory = require("../models/category.model");

// const getProductCategoryList = async () => {
//   let productCategoryList = await ProductCategory.find({}).exec();

//   if (!productCategoryList) {
//     throw new ApiError(httpStatus.BAD_REQUEST, "Failed to fetch category list");
//   }
//   return productCategoryList;
// };

const getProductCategoryList = async (page,limit,sortRule) => {
  let searchObj = {};
  searchObj.active = true; // Only return from active items
  let category = await ProductCategory.paginate(searchObj, {
    select: "_id category categoryCode",
    sort: sortRule,
    page: page,
    limit: limit,
  });
  return category;
};

const addProductCategory = async (categoryObj) => {
  let productCategory = new ProductCategory(categoryObj);
  productCategory = await productCategory.save();
  if (!productCategory)
    throw new ApiError(httpStatus.BAD_REQUEST, "Failed to add category");
  return productCategory;
};

const getProductCategory = async (categoryId) => {
  let productCategory = await ProductCategory.findOne({
    _id: categoryId,
  }).exec();

  if (!productCategory)
    throw new ApiError(httpStatus.BAD_REQUEST, "Failed to fetch category");
  return productCategory;
};

const updateProductCategory = async (categoryObj) => {
  // Store the id and delete it from the received object, to prevent any accidental replacement of id field
  let id = categoryObj._id;
  if (!id) throw new ApiError(httpStatus.BAD_REQUEST, "Missing Category id");
  delete categoryObj._id;

  // Make the update and return the updated document. Also run validators. Mongoose warns only limited validation takes place doing this in update
  let productCategory = await ProductCategory.findOneAndUpdate(
    { _id: id },
    categoryObj,
    { runValidators: true, new: true }
  ).exec();

  if (!productCategory)
    throw new ApiError(httpStatus.BAD_REQUEST, "Failed to fetch category");
  return productCategory;
};

const deleteProductCategory = async (categoryObj) => {
  // Store the id and delete it from the received object, to prevent any accidental replacement of id field
  let id = categoryObj.categoryId;
  if (!id) throw new ApiError(httpStatus.BAD_REQUEST, "Missing Category id");

  await ProductCategory.deleteOne({ _id: id }).exec();

  return {
    success: true,
    deleted: true,
  };
};

const deleteMultipleProductCategory = async (categoryIds) => {
  let productCategory = await ProductCategory.deleteMany({
    _id: { $in: categoryIds },
  }).exec();

  if (!productCategory)
    throw new ApiError(httpStatus.BAD_REQUEST, "Failed to delete categories");

  return productCategory;
};

module.exports = {
  getProductCategoryList,
  addProductCategory,
  getProductCategory,
  updateProductCategory,
  deleteProductCategory,
  deleteMultipleProductCategory,
};
