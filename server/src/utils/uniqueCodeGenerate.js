
const moment = require('moment');
const crypto = require('crypto');
const uniqueNumber = require('unique-number');
var UniqueNumber = new uniqueNumber();


const generateByDateAndTime = async () => {

    const day = moment().format('DD');
    const month = moment().format('MM');
    const year = moment().format('YYYY');
    const hour = moment().format('hh');
    const min = moment().format('mm');
    const sec = moment().format('ss');
    const key = day + month + year + hour + min + sec;
    const productcode = key + UniqueNumber.generate();

    return productcode

}

const generateRandomToken = async () => {
    return new Promise((resolve, reject) => {
        // generate reset token
        crypto.randomBytes(20, (err, buf) => {
            if (err) {
                return reject(err);
            }
            const token = buf.toString('hex');
            resolve(token);
        });
    })
};

const generateFriendlyToken = async (length = 4) => {
    return new Promise((resolve, reject) => {
        // generate reset token
        crypto.randomBytes(length, (err, buf) => {
            if (err) {
                return reject(err);
            }
            const token = buf.toString('hex');
            resolve(token);
        });
    })
}

const generateDigestRZP = async (body) => {
    //const secret = "qycJox-bobdu2-quprav";
    const secret = process.env.rzp_key_hash;

    const shasum = crypto.createHmac("sha256", secret);
    shasum.update(JSON.stringify(body));
    const digest = shasum.digest("hex");
    return digest;
}


module.exports = {
    generateByDateAndTime,
    generateRandomToken,
    generateFriendlyToken,
    generateDigestRZP
}