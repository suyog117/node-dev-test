const mongoose = require("mongoose");
const mongoosePaginator = require("mongoose-paginate");
const uniqueCode = require('../utils/uniqueCodeGenerate');

let productCategorySchema = new mongoose.Schema(
  {
    category: {
      type: String,
      required: true,
      trim: true
    },
    categoryCode: {
      type: String
    },
    slug: {
      type: String
    }
  },
  {
    timestamps: true,
  }
);
productCategorySchema.plugin(mongoosePaginator);

//add slug to sub-category
productCategorySchema.pre('save', async function (next) {
  let slugName = this.category.replace(/ /g, "-");
  this.slug = slugName.toLowerCase();
  this.categoryCode =  await uniqueCode.generateByDateAndTime();
  next();
});

productCategorySchema.index({
  category: "text"
});
const Category = mongoose.model("Category", productCategorySchema);
module.exports = Category;
