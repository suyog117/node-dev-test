const mongoose = require("mongoose");
const mongoosePaginator = require("mongoose-paginate");
const uniqueCode = require('../utils/uniqueCodeGenerate');

let productSchema = mongoose.Schema(
  {
    store: {
      type: String,
    },
    brand: {
      type: String
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    url: {
      type: String,
      required: true,
      trim: true,
    },
    sku: {
      type: String,
      required: true,
      trim: true,
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
      required: true,
      index: "text"
    },
    price: {
      type: Number,
      required: true,
    },
    weight: {
      type: Number,
      required: true,
    },
    description: {
      type: String,
      trim: true,
    },
    productCode: {
      type: Number,
      required: false
    },
    active: {
      type: Boolean,
      required: true,
      default: true,
    },
    quantity: {
      type: Number,
      default: 0
    },
    slug: {
      type: String,
    }
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
productSchema.plugin(mongoosePaginator);

//add slug to product
productSchema.pre('save', async function(next) {
  let slugName = this.name.replace(/ /g,"-");
  this.slug = slugName.toLowerCase();
  this.productCode =  await uniqueCode.generateByDateAndTime();
  next();
});

productSchema.index({
  store: "text",
  brand: "text",
  name: "text",
  sku: "text"
});

const Product = mongoose.model("Product", productSchema);

module.exports = { Product, productSchema };
