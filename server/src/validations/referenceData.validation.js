const Joi = require('joi');
const {  objectId } = require('./custom.validation');

const addProductCategory = {
    body: Joi.object().keys({
        category: Joi.string().required()
    }),
};

const getProductCategories = {
    query: Joi.object().keys({
        page: Joi.number().optional().default(1),
        limit: Joi.number().optional().default(3),
        sortRule: Joi.object().optional().default({ 'createdAt': -1 }),
    }),
};

const getProductCategory = {
    params: Joi.object().keys({
        categoryId: Joi.string().custom(objectId)
    }),
};

const updateProductCategory = {
    body: Joi.object()
        .keys({
            _id: Joi.string().custom(objectId),
            category: Joi.string().optional()
        })
        .min(1),
};

const deleteProductCategory = {
    body: Joi.object()
        .keys({
            categoryId: Joi.string().custom(objectId).required()
        })
        .min(1),
};

const deleteMultipleCategories = {
    body: Joi.object().keys({
        categoryIds: Joi.array().required(),
    }),
};


module.exports = {
    addProductCategory,
    getProductCategory,
    updateProductCategory,
    deleteProductCategory,
    deleteMultipleCategories,
    getProductCategories
};
