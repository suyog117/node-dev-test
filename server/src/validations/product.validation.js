const { object } = require('joi');
const Joi = require('joi');
const { objectId } = require('./custom.validation');

const searchCatalog = {
    query: Joi.object().keys({
        page: Joi.number().optional().default(1),
        limit: Joi.number().optional().default(3),
        searchTerm: Joi.string().optional(),
        sortRule: Joi.object().optional().default({ 'createdAt': -1 }),
        categoryIds: Joi.array().optional(),
        brand: Joi.string().optional(),
        sku: Joi.string().optional()
    }),
};

const addProduct = {
    body: Joi.object().keys({
        brand: Joi.string().optional(),
        name: Joi.string().required(),
        sku: Joi.string().required(),
        category: Joi.custom(objectId).required(),
        url: Joi.string().required(),
        price: Joi.number().required(),
        weight: Joi.number().required(),
        description: Joi.string().required()
    }),
};

const getProductDetails = {
    params: Joi.object().keys({
        productId: Joi.string().custom(objectId)
    })
};

const updateProductInCatalog = {
    body: Joi.object()
        .keys({
            _id: Joi.string().custom(objectId),
            brand: Joi.string().optional(),
            name: Joi.string().optional(),
            sku: Joi.string().optional(),
            category: Joi.custom(objectId).optional(),
            url: Joi.string().optional(),
            price: Joi.number().optional(),
            weight: Joi.number().optional(),
            description: Joi.string().optional()
        })
        .min(1),
};

const deleteProductFromCatalog = {
    body: Joi.object().keys({
        productId: Joi.string().custom(objectId),
    }),
};

const deleteMultipleProducts = {
    body: Joi.object().keys({
        productIds: Joi.array().required(),
    }),
};

module.exports = {
    addProduct,
    searchCatalog,
    getProductDetails,
    updateProductInCatalog,
    deleteProductFromCatalog,
    deleteMultipleProducts
};
