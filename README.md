# node-dev-test

basic crud operation using angular and express js framework.

# Stack used 

front-end - angular 

backend - expressjs

db - mongodb

## Getting started

Setup front end : 

1. npm install 


Setup backend :

1. npm install

2. setupr .env file in main directory that is ./server using .env.example


## Run Project 

1. ng serve

2. open another terminal , go to server folder and run in developement mode using npm run dev. to run in production mode use npm start command.


## Note : You can use docker to run mongo db. exceute below command  to run mongo db in main directory.

docker-compose up





